import { Stack, Typography } from "@mui/material";

const Home = () => {
  return (
    <div>
      <a
        href="https://www.google.com/maps/place/18%C2%B032'38.6%22N+96%C2%B036'27.5%22W/@18.5440651,-96.6102225,17z/data=!3m1!4b1!4m4!3m3!8m2!3d18.5440651!4d-96.6076476?hl=es&entry=tts"
        target="_blank"
      >
        <img
          src="https://lh3.googleusercontent.com/fife/AGXqzDm3Jbm_koK8LaVlU03wg7JhP0uY5o0c2MtFaB7NdfTlE7lV42_ypuRYisdv5K4l0LTNbhm53dq-8_31hSwCc-LH0XHoSSfAGLU_6U9BCS_yIdEPWd0VN4_pjx__guMMxTrjEpIxHWZc-iw3PMefENVYf6o3iyCkIPDhn3VIyxVP_LzP9J7bPAV-Y4LHAMz23W4SjXL2jzxAPgtz4psoh45qnKfYqqXAankhAxGReopxUlyItRbGGKkuVfy4qZxDTkxGjCmgewkFNGp25X_soCbh6-2VufizRW8bws8RFa7752BSnatsQ61Obco4c-72sz6rgO8mX6pN2v0AutT4OTudBIq-LYgpjL4R3xIdGZITjL93eK250gfEF4SOL02SeoBr_gk0qlcp_ln9BURU8udznknjWEjYZMkKlzdU1-reV3yqmNaFf5JDTsxIF5wn_LexkxYo_R2GRAZ7xF77Vq6RSBrGv_oTj4XNVmI8Yt_NhJUZAQhDdAC3FlLd4-NZTX8vOmikH6Lm6etKpBBT0f0fBjLIKKprFYas9c_L0S-bXSAc_r6BOCk7mGmLphziEfUuowGNs4QRbl-V21W4nMgVzEJh8PtegFS8HyI98K1pEm7ADcyTc5EwFykKHtUtuC6nL7ErAmxMWdLNGZ4EjA96eGpg_pMV6AZNb5IB0dAWhmL_YJVWzZuq52m-0JEAdAhn2BCuXB8wQ2s5NTVxemHpBb-WtrsTmaOvhhJry2qQ4XsgpVJy25ZiqhJ_rE_bU1MIHJL0vJbNPBSdXc1V0lt14EwU5-Fq-Qj5tpzVs68K_cXrL5Gjuiq40EwwChpD32YthxmxOS6TCindDRh3rgggb2W4aAlzExQtppeVdmIkikrpMNL13iNEmZti2O1IjSEBTJEx_2h5eiPPvzlTC65KNII6hn1EeCKDKEHGNDIn9eLfjvNWIbIKw5gZXiV5s9k9a3z2jAYwfiT9Aioi7VDbB-lIHNeKW8wCEztYWNu1eNts6QCIcwSdrypke1ObDEKk5WWrrUzdy0xnYWenI_6RIJqqPM2Mp200AcfKSefoKGo0oNdC_GJgVBhQcb0C9DkBjXZSLOjQxMuGT2imEZxsL3YE-JjDKGHHGrNyDA5pQYKrQMYnKqhhyoXpsQm5iiVR20PxockUVcDg4A8ajEjE9vHc-v83iA2psRCDmI_hKXHDRxkUkgVUM7MwZrnuHev6rN3Ki33ED_vyhjRHNMRwKoaaETSN1d3NfcfU3_IgNDikY9__jun8CC6ivaRjn4B6nWQbY5fbDu6KSrBK-dT7l9awuVcfdzitRGVRHT1qzLSyB5tVTRg-GuVXgLCYLsV-ps6dPkfi2QrFjPg6pIk3YgIJ494aCt2ccWyToFY0t2v-Ndg0U4f7tnrkGdl3lZ3X1lidW-lP3xeCC9t9TjbGt9zUi2STVzWHBsqKfo-i8wXGErgTvDIz-cEnts6TgNxwkvlADobL4aAXfxssmUub4S69VFvtmWfSZem3fFY-2IMxuAGN9OIQjwwK6E7fltrAKe7lBJvRofXyzp1qZuNTFdgItg3n=w751-h583"
          alt="lissito bebe"
          width="100%"
          height="100%"
        />
      </a>
    </div>
  );
};

export default Home;
